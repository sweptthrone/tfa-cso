ENT.Type 			= "anim"
ENT.Base 			= "base_anim"
ENT.PrintName		= "Burning AUG Grenade"
ENT.Category		= "None"

ENT.Spawnable		= false
ENT.AdminSpawnable	= false
ENT.DoNotSpawn = false
ENT.ProjectileVel = Vector(0, 0, 0)
ENT.SpreadYaw = 5
ENT.Spawned = false

ENT.MyModel = "models/weapons/tfa_cso/w_shell_svdex.mdl"
ENT.MyModelScale = 0.35
ENT.Damage = 480
ENT.Radius = 225
if SERVER then

	AddCSLuaFile()

	function ENT:Initialize()

		local model = self.MyModel and self.MyModel or "models/weapons/tfa_cso/w_shell_svdex.mdl"
		
		self.Class = self:GetClass()
		
		self:SetModel(model)
		util.SpriteTrail(self, 0, Color(255,255,255), false, 7, 1, 0.5, 0.125, "trails/smoke.vmt")
		self:PhysicsInit(SOLID_VPHYSICS)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)
		self:DrawShadow(true)
		self:SetCollisionGroup(COLLISION_GROUP_NONE)
		self:SetHealth(1)
		self:SetModelScale(self.MyModelScale,0)
		
		local phys = self:GetPhysicsObject()
		
		if (phys:IsValid()) then
			phys:Wake()
		end
		if(self.DoNotSpawn) then return end
		timer.Simple(0.3, function()
			if(!IsValid(self)) then return end
			self.Spawned = true
			local baseAngle = self:GetAngles() - Angle(0, self.SpreadYaw, 0)
			for i = 1, 3, 1 do
				local angle = baseAngle + Angle(0, self.SpreadYaw * (i - 1), 0)
				local nade = ents.Create(self:GetClass())
					nade.DoNotSpawn = true
					nade:SetPos(self:GetPos())
					nade:SetAngles(angle)
					nade:SetOwner(self:GetOwner())
					nade:Spawn()
					nade:Activate()
					nade:GetPhysicsObject():SetVelocity(angle:Forward() * self.ProjectileVel)
			end
			local owent = self.Owner and self.Owner or self
			util.BlastDamage(self,owent,self:GetPos(),self.Radius,self.Damage)
			local fx = EffectData()
			fx:SetOrigin(self:GetPos())
			util.Effect("exp_grenade",fx)
			self:Remove()
		end)
	end

	function ENT:PhysicsCollide(data, physobj)
		if(!self.Spawned && !self.DoNotSpawn) then
			local baseAngle = self:GetAngles() - Angle(0, self.SpreadYaw, 0)
			for i = 1, 3, 1 do
				local angle = baseAngle + Angle(0, self.SpreadYaw * (i - 1), 0)
				local nade = ents.Create(self:GetClass())
					nade.DoNotSpawn = true
					nade:SetPos(self:GetPos())
					nade:SetAngles(angle)
					nade:SetOwner(self:GetOwner())
					nade:Spawn()
					nade:Activate()
					nade:GetPhysicsObject():SetVelocity(angle:Forward() * self.ProjectileVel)
			end
		end
		local owent = self.Owner and self.Owner or self
		util.BlastDamage(self,owent,self:GetPos(),self.Radius,self.Damage)
		local fx = EffectData()
		fx:SetOrigin(self:GetPos())
		util.Effect("exp_grenade",fx)
		self:Remove()
	end
end

if CLIENT then
	
	function ENT:Draw()
		self:DrawModel()
	end

end