SWEP.Base = "tfa_melee_base"
SWEP.Category = "TFA CS:O Melees"
SWEP.PrintName = "TURBULENT-9"
SWEP.Author	= "★Bullet★" --Author Tooltip
SWEP.ViewModel = "models/weapons/tfa_cso/c_turbulent9.mdl"
SWEP.WorldModel = "models/weapons/tfa_cso/w_turbulent9_r.mdl"
SWEP.ViewModelFlip = false
SWEP.ViewModelFOV = 80
SWEP.UseHands = true
SWEP.HoldType = "fist"
SWEP.Type	= "Rare Grade Melee"
SWEP.DrawCrosshair = true

SWEP.Primary.Directional = false

SWEP.Spawnable = true
SWEP.AdminOnly = false
SWEP.Secondary.Automatic = true
SWEP.DisableIdleAnimations = false

SWEP.Secondary.CanBash = false
SWEP.Secondary.MaxCombo = -1
SWEP.Primary.MaxCombo = -1

SWEP.VMPos = Vector(0,0,0) --The viewmodel positional offset, constantly.  Subtract this from any other modifications to viewmodel position.

--[[INSPECTION]]--

SWEP.InspectPos = Vector (0,0,0) --Replace with a vector, in style of ironsights position, to be used for inspection
SWEP.InspectAng = Vector (0,0,0) --Replace with a vector, in style of ironsights angle, to be used for inspection
SWEP.InspectionLoop = true --Setting false will cancel inspection once the animation is done.  CS:GO style.

-- nZombies Stuff
SWEP.NZWonderWeapon		= false	-- Is this a Wonder-Weapon? If true, only one player can have it at a time. Cheats aren't stopped, though.
--SWEP.NZRePaPText		= "your text here"	-- When RePaPing, what should be shown? Example: Press E to your text here for 2000 points.
SWEP.NZPaPName				= "TURBO-BLAZED-18"
--SWEP.NZPaPReplacement 	= "tfa_cso_dualinfinityfinal"	-- If Pack-a-Punched, replace this gun with the entity class shown here.
SWEP.NZPreventBox		= false	-- If true, this gun won't be placed in random boxes GENERATED. Users can still place it in manually.
SWEP.NZTotalBlackList	= false	-- if true, this gun can't be placed in the box, even manually, and can't be bought off a wall, even if placed manually. Only code can give this gun.

SWEP.WElements = {
	["turbulent9_l"] = { type = "Model", model = "models/weapons/tfa_cso/w_turbulent9_l.mdl", bone = "ValveBiped.Bip01_L_Hand", rel = "", pos = Vector(2.5, 1, 0), angle = Angle(180, 180, 120), size = Vector(1.2, 1.2, 1.2), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.Offset = {
		Pos = {
		Up = -1,
		Right = 0,
		Forward = 2.5,
		},
		Ang = {
		Up = -190,
		Right = 190,
		Forward = 75
		},
		Scale = 1.2
}


sound.Add({
	['name'] = "TURBULENT9.Draw",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/turbulent9/draw.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "TURBULENT9.Idle",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/turbulent9/idle.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "TURBULENT9.Slash1",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/turbulent9/slash1.wav"},
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "TURBULENT9.Slash2",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/turbulent9/slash2.wav"},
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "TURBULENT9.HitFlesh",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/turbulent9/hit1.wav", "weapons/tfa_cso/knife/hit2.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "TURBULENT9.Stab",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/turbulent9/stab.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "TURBULENT9.HitWall",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/turbulent9/hitwall1.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "TURBULENT9.Slash3_Ready",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/turbulent9/slash3_ready.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "TURBULENT9.Slash3_Idle",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/turbulent9/slash3_idle.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "TURBULENT9.Slash3_Hit_Idle",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/turbulent9/slash3_hit_idle.wav" },
	['pitch'] = {100,100}
})

SWEP.Primary.Attacks = {
	{
		['act'] = ACT_VM_HITLEFT, -- Animation; ACT_VM_THINGY, ideally something unique per-sequence
		['len'] = 13*5, -- Trace source; X ( +right, -left ), Y ( +forward, -back ), Z ( +up, -down )
		['dir'] = Vector(60,0,0), -- Trace dir/length; X ( +right, -left ), Y ( +forward, -back ), Z ( +up, -down )
		['dmg'] = 45, --This isn't overpowered enough, I swear!!
		['dmgtype'] = DMG_SLASH, --DMG_SLASH,DMG_CRUSH, etc.
		['delay'] = 0.18, --Delay
		['spr'] = true, --Allow attack while sprinting?
		['snd'] = "TURBULENT9.Slash1", -- Sound ID
		['snd_delay'] = 0.1,
		["viewpunch"] = Angle(0,0,0), --viewpunch angle
		['end'] = 0.65, --time before next attack
		['hull'] = 48, --Hullsize
		['direction'] = "W", --Swing dir,
		['hitflesh'] = "TURBULENT9.HitFlesh",
		['hitworld'] = "TURBULENT9.HitWall"
	},
	{
		['act'] = ACT_VM_HITRIGHT, -- Animation; ACT_VM_THINGY, ideally something unique per-sequence
		['len'] = 13*5, -- Trace source; X ( +right, -left ), Y ( +forward, -back ), Z ( +up, -down )
		['dir'] = Vector(50,0,-70), -- Trace dir/length; X ( +right, -left ), Y ( +forward, -back ), Z ( +up, -down )
		['dmg'] = 45, --This isn't overpowered enough, I swear!!
		['dmgtype'] = DMG_SLASH, --DMG_SLASH,DMG_CRUSH, etc.
		['delay'] = 0.15, --Delay
		['spr'] = true, --Allow attack while sprinting?
		['snd'] = "TURBULENT9.Slash2", -- Sound ID
		['snd_delay'] = 0.1,
		["viewpunch"] = Angle(0,0,0), --viewpunch angle
		['end'] = 0.65, --time before next attack
		['hull'] = 48, --Hullsize
		['direction'] = "W", --Swing dir,
		['hitflesh'] = "TURBULENT9.HitFlesh",
		['hitworld'] = "TURBULENT9.HitWall"
	}
}

DEFINE_BASECLASS(SWEP.Base)

ENUM_PREPOKE = 1
ENUM_POKING = 2
ENUM_ENDPOKE = 3

SWEP.Poking = false
SWEP.PrePokeTime = 0
SWEP.PrePokeLoopTime = 0
SWEP.PokingTime = 0
SWEP.EndPokeTime = 0
SWEP.PokeAttackTime = 0
SWEP.PokeEndTime = 0
SWEP.PokeStatus = -1

SWEP.PokeDamage = 60

function SWEP:PrimaryAttack(...)
	BaseClass.PrimaryAttack(self, ...)
end

function SWEP:SecondaryAttack()
	return
end

function SWEP.Holster(self)
	self.Poking = false
	self.PokeStatus = -1
	return true
end

function SWEP:Deploy(...)
	self.Poking = false
	self.PokeStatus = -1
	BaseClass.Deploy(self, ...)
end

SWEP.M2Down = false
function SWEP:Think2(...)
	if(!CSO:BlockExtraActionTime(self)) then
		local m2 = self.Owner:KeyDown(2048)
		if(m2) then
			if(!self.M2Down) then
				self.Owner:EmitSound("weapons/tfa_cso/turbulent9/slash3_ready.wav")
				CSO:SetNextActionTimeIdle(self, 32767)
				CSO:ForceVMSequenceNoStatus(self, 4)
				self.PrePokeTime = CurTime() + 0.55
				self.PokeStatus = 1
			end
		end
		self.M2Down = m2
	end
	local hold = self.Owner:KeyDown(2048)
	if(self.PokeStatus == 1) then
		if(self.PrePokeTime < CurTime()) then
			if(hold) then
				if(self.PrePokeTime < CurTime()) then
					if(self.PrePokeLoopTime < CurTime()) then
						self:EmitSound("weapons/tfa_cso/turbulent9/slash3_idle.wav")
						CSO:ForceVMSequenceNoStatus(self, 5)
						self.PrePokeLoopTime = CurTime() + 0.4
					end
				end
			else
				self.PokeStatus = 2
				CSO:ForceVMSequenceNoStatus(self, 7)
				self.PokingTime = CurTime() + (0.35 * 3) + 0.1
			end
		end
	elseif(self.PokeStatus == 2) then
		if(self.PokeAttackTime < CurTime()) then
			CSO:QuickMeleeTrace(self, 3, Vector(6, 6, 1), 256, self.PokeDamage, 110, "weapons/tfa_cso/turbulent9/hit2.wav", "weapons/tfa_cso/turbulent9/hitwall1.wav")
			self:EmitSound("weapons/tfa_cso/turbulent9/slash3_hit_idle.wav")
			self.PokeAttackTime = CurTime() + 0.385
		end
		if(self.PokingTime < CurTime()) then
			CSO:ForceVMSequenceNoStatus(self, 8)
			self.PokeStatus = 3
			self.PokeEndTime = CurTime() + 0.33
		end
	elseif(self.PokeStatus == 3) then
		if(self.PokeEndTime < CurTime()) then
			self.PokeStatus = -1
			CSO:SetNextActionTimeIdle(self, 0)
		end
	end

	BaseClass.Think2(self, ...)
end