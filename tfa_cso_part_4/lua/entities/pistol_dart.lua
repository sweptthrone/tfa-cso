ENT.Type 			= "anim"
ENT.Base 			= "base_anim"
ENT.PrintName		= "AT4_Rocket"
ENT.Category		= "None"

ENT.Spawnable		= false
ENT.AdminSpawnable	= false

ENT.ImpactEntity = nil
ENT.HitEntity = false
ENT.HitOffset = Vector(0, 0, 0)

ENT.BaseAngle = Angle(0, 0, 0)
ENT.Impacted = false
ENT.ImpactedVector = Vector(0, 0, 0)

ENT.ShockInterval = 0.9

ENT.MaxShockCount = 5
ENT.ShockCurTime = -1

ENT.ShockDamage = 35

ENT.Wep = nil

if SERVER then
	AddCSLuaFile()
	function ENT:Initialize()
		self.BaseAngle = self:GetAngles()
		self.Wep = self.Owner:GetActiveWeapon()
		util.SpriteTrail(self, 0, Color(200,200,200), false, 0.3, 0.2, 0.2, 0.1, "trails/smoke.vmt")
		self.Class = self:GetClass()
		
		self:PhysicsInit(SOLID_VPHYSICS)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)
		self:DrawShadow(true)
		self:SetCollisionGroup(COLLISION_GROUP_NONE)
		local phys = self:GetPhysicsObject()
		
		if (phys:IsValid()) then
			phys:Wake()
			phys:SetMass(1)
			phys:EnableDrag(false)
			phys:EnableGravity(false)
			phys:SetBuoyancyRatio(0)
		end
		self:SetGravity(0.2)
	end

	function ENT:PhysicsCollide(data, physobj)
		local pos = data.HitPos
		local ent = data.HitEntity
		if(IsValid(ent)) then
			self.ImpactEntity = ent
			self.HitEntity = true
			self.HitOffset = pos - ent:GetPos()
		else
			self.ImpactedVector = pos
			self:SetPos(self.ImpactedVector)
		end
		local phys = self:GetPhysicsObject()
		if(IsValid(phys)) then
			phys:EnableCollisions(false)
			phys:EnableMotion(false)
		end
		self:SetCollisionGroup(1)
		self:SetAngles(self.BaseAngle)
		self.Impacted = true
	end
	
	function ENT:Think()
		if(!IsValid(self.Owner) || !IsValid(self.Wep)) then self:Remove() return end
		if(self.Impacted) then
			if(self.HitEntity) then
				if(IsValid(self.ImpactEntity)) then
					self:SetPos(self.ImpactEntity:GetPos() + self.HitOffset)
				end
			else
				self:SetPos(self.ImpactedVector)
			end
			if(self.ShockCurTime < CurTime()) then
				local e = EffectData()
				e:SetOrigin(self:GetPos())
				util.Effect("exp_dartpistol", e)
				CSO:DoRadiusAttack_WeaponLess(self.Owner, CSO:DMGInfo(self.Owner, self.Wep, self.ShockDamage, Vector(0, 0, 0), DMG_SHOCK, Vector(0, 0, 0)), 86, false, 0, 1, true, self:GetPos(), false, true, 1, 0.7)
				self.MaxShockCount = self.MaxShockCount - 1
				self.ShockCurTime = CurTime() + self.ShockInterval
			end
			if(self.MaxShockCount <= 0) then
				self:Remove()
			end
		end
		self:NextThink(CurTime())
		return true
	end
end

if CLIENT then
	function ENT:Draw()
		self:DrawModel()
	end
end